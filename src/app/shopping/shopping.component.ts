import { UserService } from './../service/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css'],
  providers: [UserService],
})
export class ShoppingComponent implements OnInit {
  posts: any[] = [];
  constructor(private readonly userService: UserService) {}

  ngOnInit(): void {
    this.userService.getPosts().subscribe((posts) => {
      this.posts = posts;
      console.log(this.posts);
    });
  }

  selectProduct(event: any) {
    alert('you have selected product with id : ' + event.id);
  }
}
