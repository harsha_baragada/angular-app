import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy
{
  @Input() inputPost: any;
  @Output() selectedProduct = new EventEmitter<any>();

  constructor() {
    console.log('CardComponent :: constructor');
  }

  ngOnInit(): void {
    console.log('CardComponent :: ngOnInit ');
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('CardComponent :: ngOnChanges ');
  }

  ngDoCheck() {
    console.log('CardComponent :: ngDoCheck ');
  }

  ngAfterContentInit() {
    console.log('CardComponent :: ngAfterContentInit ');
  }

  ngAfterContentChecked() {
    console.log('CardComponent :: ngAfterContentChecked ');
  }

  ngAfterViewInit() {
    console.log('CardComponent :: ngAfterViewInit ');
  }

  ngAfterViewChecked() {
    console.log('CardComponent :: ngAfterViewChecked ');
  }

  selectProduct(product: any) {
    this.selectedProduct.emit(product);
  }

  ngOnDestroy() {
    console.log('CardComponent :: ngOnDestroy ');
  }
}
