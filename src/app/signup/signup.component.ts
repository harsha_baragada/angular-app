import { UserService } from './../service/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [UserService],
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  constructor(private readonly userService: UserService) {}

  ngOnInit(): void {
    this.signupForm = this.createSignupForm();
  }

  createSignupForm(): FormGroup {
    return new FormGroup({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordControl: new FormControl('', [Validators.required]),
    });
  }

  signUp() {
    if (this.signupForm.valid) {
      let user = {
        emailId: this.signupForm.value.emailFormControl,
        pwd: this.signupForm.value.passwordControl,
      };

      this.userService.userSignUp(user).subscribe((response) => {
        if ((response.body = '200')) {
          alert('sign up is successfull .!!');
        } else if ((response.body = '201')) {
          alert('user already exists');
        }
      });
    }
  }
}
