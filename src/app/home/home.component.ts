import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title = 'This is my first angular application';
  userName: string = 'This is  two way binding ';
  isFormVisible!: boolean;
  btnTitle: string = 'Show form';
  cities: string[] = [
    'Mumbai',
    'Hyderabad',
    'Bengaluru',
    'Delhi',
    'Kolkata',
    'Gurgoan',
    'Noida',
  ];
  constructor() {}

  ngOnInit(): void {}
  submit() {
    alert(this.userName);
  }
  toggleForm() {
    this.isFormVisible = !this.isFormVisible;
    if (this.isFormVisible) this.btnTitle = 'Hide Form';
    else this.btnTitle = 'Show form';
  }
}
